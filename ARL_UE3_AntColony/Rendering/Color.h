#pragma once

class Color
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
private:
	// Holds the values
	float _rgba[4];

public:
	Color(void);
	explicit Color(float u);
	Color(float u, float a);
	Color(float r, float g, float b);
	Color(float r, float g, float b, float a);

	~Color(void) { }

	// Get color as array
	const float* const Get() const { return _rgba; }

	// Get red component
	inline float R() const { return _rgba[0]; }
	// Get green component
	inline float G() const { return _rgba[1]; }
	// Get blue component
	inline float B() const { return _rgba[2]; }
	// Get alpha component
	inline float A() const { return _rgba[3]; }

	// Set red component
	inline void R(float r) { _rgba[0] = r; }
	// Set green component
	inline void G(float g) { _rgba[1] = g; }
	// Set blue component
	inline void B(float b) { _rgba[2] = b; }
	// Set alpha component
	inline void A(float a) { _rgba[3] = a; }

	// Set RGB component to same value
	inline void Set(float u) { _rgba[0] = u; _rgba[1] = u; _rgba[2] = u; }
	// Set RGB component to same value and an alpha component
	inline void Set(float u, float a) { _rgba[0] = u; _rgba[1] = u; _rgba[2] = u; _rgba[3] = a; }
	// Set RGB component
	inline void Set(float r, float g, float b) { _rgba[0] = r; _rgba[1] = g; _rgba[2] = b; }
	// Set RGBA component
	inline void Set(float r, float g, float b, float a) { _rgba[0] = r; _rgba[1] = g; _rgba[2] = b; _rgba[3] = a; }



public:
	static Color s_black;
	static Color s_white;
	static Color s_red;
	static Color s_green;
	static Color s_blue;
	static Color s_yellow;
	static Color s_magenta;
	static Color s_cyan;
	static Color s_grey;
	static Color s_orange;

	static Color s_lightBlue;
	static Color s_lightRed;
};

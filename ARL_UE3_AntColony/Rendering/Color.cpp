//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "Color.h"




//--------------------------------------------------------------------------------------
// Statics
//--------------------------------------------------------------------------------------
Color Color::s_black = Color(0.0f, 0.0f, 0.0f);
Color Color::s_white = Color(1.0f, 1.0f, 1.0f);
Color Color::s_red = Color(1.0f, 0.0f, 0.0f);
Color Color::s_green = Color(0.0f, 1.0f, 0.0f);
Color Color::s_blue = Color(0.0f, 0.0f, 1.0f);
Color Color::s_yellow = Color(1.0f, 1.0f, 0.0f);
Color Color::s_magenta = Color(1.0f, 0.0f, 1.0f);
Color Color::s_cyan = Color(0.0f, 1.0f, 1.0f);
Color Color::s_grey = Color(0.5f, 0.5f, 0.5f);


Color Color::s_orange = Color(1.0f, 0.5f, 0.0f);
Color Color::s_lightBlue = Color(0.5f, 0.5f, 1.0f);
Color Color::s_lightRed = Color(1.0f, 0.8f, 0.8f);

//--------------------------------------------------------------------------------------
// Methods
//--------------------------------------------------------------------------------------
Color::Color(void)
{
	Set(1.0f, 0.0f); // White
}

Color::Color(float u)
{
	Set(u, 0.0f);
}

Color::Color(float u, float a)
{
	Set(u, a);
}

Color::Color(float r, float g, float b)
{
	Set(r, g, b, 0.0f);
}

Color::Color(float r, float g, float b, float a)
{
	Set(r, g, b, a);
}

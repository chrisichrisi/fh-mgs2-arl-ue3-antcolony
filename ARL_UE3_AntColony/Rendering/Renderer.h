#pragma once
//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "include.h"


template <class SIMULATOR>
class Renderer
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
private:
	GLFWwindow* _window;
	float _z;

public:
	void Init(GLFWwindow* window, const Vector2i& windowSize)
	{
		_window = window;
		_z = -37.0f;

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClearDepth(1.0);
		glDepthFunc(GL_LESS);
		glEnable(GL_DEPTH_TEST);
		glShadeModel(GL_SMOOTH);


		Resize(windowSize);
	}


	void Resize(const Vector2i& windowSize)
	{
		glViewport(0, 0, windowSize.X, windowSize.Y);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(45.0f, (GLfloat)windowSize.X / (GLfloat)windowSize.Y, 0.1f, 100.0f);

		glMatrixMode(GL_MODELVIEW);
	}



	void Render(SIMULATOR& simulator)
	{
		BeginScene();
		RenderScene(simulator);
		EndScene();
	}


	float Z() const { return _z; }
	void Z(float val) { _z = val; }

private:
	void BeginScene()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
	}
	void RenderScene(SIMULATOR& simulator)
	{
		//glTranslatef(0.0f, 0.0f, _z);
		glTranslatef(0.7f, 1.0f, _z);
		//RenderHexMap();
		simulator.Render();
	}
	void EndScene()
	{
		// Swap front and back buffers
		glfwSwapBuffers(_window);
	}
};


#include "RenderHelper.h"
#include "include.h"

#ifndef M_PI 
#define M_PI    3.14159265358979323846f 
#endif





// STATIC HELPERS

void RenderHelper::RenderRS(RenderShape rs, const Vector2f& pos, const Color& c, const Vector2f& scale)
{
	glPushMatrix();
	{
		glTranslatef(pos.X, pos.Y, 0.0f);
		//glRotatef(_rotation, 0, 0, 1);
		glScalef(scale.X, scale.Y, 0.0f);
		glColor4f(c.R(), c.G(), c.B(), c.A());


		if (rs == RS_POINT) { RenderHelper::RenderPointByVertex(); }
		else if (rs == RS_QUAD) { RenderHelper::RenderQuadByVertices(); }
		else if (rs == RS_TRIANGLE) { RenderHelper::RenderTriangleByVertices(); }
		else if (rs == RS_HEXAGON) { RenderHelper::RenderHexagonByVertices(c); }
		else if (rs == RS_CIRCLE) { RenderHelper::RenderCircleByVertices(20); }
	}
	glPopMatrix();
}







void RenderHelper::RenderHexagon(const Vector2f& pos, const Color& c)
{
	RenderRS(RS_HEXAGON, pos, c);
}

void RenderHelper::RenderTriangle(const Vector2f& pos, const Color& c)
{
	RenderRS(RS_TRIANGLE, pos, c);
}


void RenderHelper::RenderQuad(const Vector2f& pos, const Color& c)
{
	RenderRS(RS_QUAD, pos, c);
}





void RenderHelper::RenderHexMapTile(const MapTile::Enum& tile, const Vector2f& pos)
{
	Color c;
	/*if (tile == MapTile::MT_DIRT)			{ c = Color::s_orange; }
	else if (tile == MapTile::MT_GRASS)		{ c = Color::s_green; }
	else if (tile == MapTile::MT_ASPHALT)	{ c = Color::s_grey; }
	else									{ c = Color::s_black; }
*/

	//if (tile == MapTile::MT_DIRT)			{ c = Color::s_orange; }
	if (tile == MapTile::Grass)		{ c = Color::s_green; }
	else if (tile == MapTile::Nest)	{ c = Color::s_orange; }
	else if (tile == MapTile::Food)	{ c = Color::s_blue; }
	else if (tile == MapTile::Unpassable)	{ c = Color::s_black; }

	RenderHelper::RenderHexagon(pos, c);
}


void RenderHelper::RenderCircle(const Vector2f& pos, const Color& c)
{
	RenderRS(RS_CIRCLE, pos, c, Vector2f(0.25f, 0.25f));
}


void RenderHelper::RenderPoint(const Vector2f& pos, const Color& c)
{
	glPointSize(20.0f);
	glColor4f(c.R(), c.G(), c.B(), c.A());
	glBegin(GL_POINTS);
	{
		glVertex3f(pos.X, pos.Y, 0.0f);
	}
	glEnd();
}





void RenderHelper::RenderPointByVertex()
{
	glPointSize(20.0f);
	glBegin(GL_POINTS);
	{
		//glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex2f(0.0f, 0.0f);
	}
	glEnd();
}




void RenderHelper::RenderTriangleByVertices()
{
	glBegin(GL_POLYGON);
	{
		glVertex3f(0.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
	}
	glEnd();
}

void RenderHelper::RenderQuadByVertices()
{
	glBegin(GL_QUADS);
	{
		glVertex3f(-1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);
	}
	glEnd();
}

void RenderHelper::RenderHexagonByVertices(const Color& c)
{
	RenderCircleLineByVertices(6);
	glColor4f(c.R(), c.G(), c.B(), c.A());
	RenderCircleByVertices(6);
}

void RenderHelper::RenderCircleByVertices(int points)
{
	float p = (float)points;
	
	glBegin(GL_POLYGON);
	{
		for (int i = 0; i < points; ++i)
		{
			float ay = (float)sin((i * 2.0f * M_PI) / p);
			float ax = (float)cos((i * 2.0f * M_PI) / p);
			glVertex3f(ax, ay, 0.0f);
		}
	}
	glEnd();
}


void RenderHelper::RenderCircleLineByVertices(int points)
{
	float p = (float)points;
	glColor3f(1.0f, 1.0f, 1.0f);

	float lineWidth = 5.0f; // 3.0f
	glLineWidth(lineWidth);
	glBegin(GL_LINE_LOOP);
	{
		for (int i = 0; i < points; ++i)
		{
			float ay = (float)sin((i * 2.0f * M_PI) / p);
			float ax = (float)cos((i * 2.0f * M_PI) / p);
			glVertex3f(ax, ay, 0.0f);
		}
	}
	glEnd();
}




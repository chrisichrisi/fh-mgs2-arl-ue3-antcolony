//************************************
// Includes
//************************************
#include "include.h"
#include "Simulation.h"
#include "AntColony/AntColonySim.h"


int main(int arc, int argv)
{
	GED_NEW_LOGGER(logger, PlainConsoleLogger);
	GED_NEW_LOGGER(ideOutputLogger, DebugIDELogger);

	GED_LOG(LogVerbosity::Info, 1, "Initialize");
	Simulation<AntColonySim> sim;

	if (sim.Init() == G_OK)
	{
		GED_LOG(LogVerbosity::Info, 1, "Starting main loop");
		sim.Run();
	}
	GED_LOG(LogVerbosity::Info, 1, "Shutting down...");

	return 0;
}


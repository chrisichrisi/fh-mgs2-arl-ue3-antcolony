#pragma once
//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include <map>
#include "include.h"


class MouseState
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
private:
	std::map<int, char> _mouseButtons;
	Vector2i _screenPos;

public:
	inline void SetScreenPosition(Vector2i pos) { _screenPos = pos; }
	inline Vector2i GetScreenPosition() const { return _screenPos; }
	inline Vector2f GetWorldPosition() const { return MouseState::ScreenToWorldPos(_screenPos); }

	void SetButton(int key, char state);
	char GetButton(int key) const;

	inline bool IsPressed(int key) const { return GetButton(key) == GLFW_PRESS; }
	inline bool IsReleased(int key) const { return GetButton(key) == GLFW_RELEASE; }

	static Vector2f ScreenToWorldPos(const Vector2i& screenPos);
};


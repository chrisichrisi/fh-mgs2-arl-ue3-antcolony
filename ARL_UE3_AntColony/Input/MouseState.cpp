//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "MouseState.h"

void MouseState::SetButton(int key, char state)
{
	_mouseButtons[key] = state;
}

char MouseState::GetButton(int key) const
{
	auto it = _mouseButtons.find(key);
	if (it == _mouseButtons.end())
	{
		return GLFW_RELEASE;
	}
	return it->second;
}



Vector2f MouseState::ScreenToWorldPos(const Vector2i& screenPos)
{
	GLint viewport[4]; //var to hold the viewport info
	GLdouble modelview[16]; //var to hold the modelview info
	GLdouble projection[16]; //var to hold the projection matrix info
	//GLint winX, winY, winZ; //variables to hold screen x,y,z coordinates
	GLint winX, winY; //variables to hold screen x,y,z coordinates
	GLfloat winZ;
	GLdouble worldX, worldY, worldZ; //variables to hold world x,y,z coordinates

	glGetDoublev(GL_MODELVIEW_MATRIX, modelview); //get the modelview info
	glGetDoublev(GL_PROJECTION_MATRIX, projection); //get the projection matrix info
	glGetIntegerv(GL_VIEWPORT, viewport); //get the viewport info

	winX = screenPos.X;
	winY = viewport[3] - screenPos.Y;
	glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &winZ);
	//glReadPixels(winX, winY, 1, 1, GL_DEPTH_COMPONENT, GL_INT, &winZ);

	//winZ = 0.0f;
	//get the world coordinates from the screen coordinates
	gluUnProject(winX, winY, winZ, modelview, projection, viewport, &worldX, &worldY, &worldZ);

	return Vector2f((float)worldX, (float)worldY);
}

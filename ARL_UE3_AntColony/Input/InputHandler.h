#pragma once
//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "include.h"
#include "MouseState.h"
#include "KeyboardState.h"


class InputHandler
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
private:
	MouseState _mouse;
	KeyState _keyboard;

	MouseState _oldMouse;
	KeyState _oldKeyboard;



public:
	void Init(GLFWwindow* window);
	void Update(float dt);

	inline MouseState GetMouseState() const { return _mouse; }
	inline KeyState GetKeyState() const { return _keyboard; }


	inline bool KeyChangedToDown(int key) const { return (_keyboard.IsPressed(key) && _oldKeyboard.IsReleased(key)); }
	inline bool MouseButtonChangedToDown(int key) const { return (_mouse.IsPressed(key) && _oldMouse.IsReleased(key)); }


	static InputHandler* GetInstance()
	{
		if (s_instance == NULL) { s_instance = new InputHandler(); }
		return s_instance;
	}

private:

	static void KeyEventCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void MouseButtonCallback(GLFWwindow* window, int mouseButton, int action, int mods);
	static void MousePosCallback(GLFWwindow* window, double posX, double posY);

	static InputHandler* s_instance;
};

#pragma once
//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include <map>
#include "include.h"


class KeyState
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
private:
	std::map<int, char> _keyStates;
public:

	void Set(int key, char state);
	char Get(int key) const;

	inline bool IsPressed(int key) const { return Get(key) != GLFW_RELEASE; }
	inline bool IsReleased(int key) const { return Get(key) == GLFW_RELEASE; }
};


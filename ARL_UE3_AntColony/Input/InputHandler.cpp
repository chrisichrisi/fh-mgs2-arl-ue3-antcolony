/********************************************************************
	created:	2014/02/07
	created:	7:2:2014   11:27
	filename: 	E:\Projects\FH\MGS\MGS1\KI\Pathfindung\Pathfindung\Input\InputHandler.cpp
	file path:	E:\Projects\FH\MGS\MGS1\KI\Pathfindung\Pathfindung\Input
	file base:	InputHandler
	file ext:	cpp
	author:		Christoph Kappler
	
	purpose:	Used for input handling
*********************************************************************/


//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "InputHandler.h"
#include <iostream>

InputHandler* InputHandler::s_instance = NULL;


void InputHandler::Init(GLFWwindow* window)
{

	glfwSetKeyCallback(window, KeyEventCallback);
	glfwSetMouseButtonCallback(window, MouseButtonCallback);
	glfwSetCursorPosCallback(window, MousePosCallback);
}


void InputHandler::Update(float dt)
{
	_oldMouse = MouseState(_mouse);
	_oldKeyboard = KeyState(_keyboard);
}



void InputHandler::KeyEventCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
	s_instance->_keyboard.Set(key, (char)action);
}

void InputHandler::MouseButtonCallback(GLFWwindow* window, int mouseButton, int action, int mods)
{
	s_instance->_mouse.SetButton(mouseButton, (char)action);
}

void InputHandler::MousePosCallback(GLFWwindow* window, double posX, double posY)
{
	s_instance->_mouse.SetScreenPosition(Vector2i((int) posX, (int) posY));
}

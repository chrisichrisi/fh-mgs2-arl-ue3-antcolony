//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "KeyboardState.h"


void KeyState::Set(int key, char state)
{
	_keyStates[key] = state;
}

char KeyState::Get(int key) const
{
	auto it = _keyStates.find(key);
	if (it == _keyStates.end())
	{
		return GLFW_RELEASE;
	}
	return it->second;
}

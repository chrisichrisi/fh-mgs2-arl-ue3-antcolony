#pragma once
//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "Rendering\Renderer.h"
#include "Input\InputHandler.h"
#include "Core\Timer.h"






template <class SIMULATOR>
class Simulation
{
	//Members
private:
	SIMULATOR m_sim;
	Renderer<SIMULATOR> m_renderer;
	Timer m_timer;
	InputHandler* m_inputHandler;

	GLFWwindow* _window;

	//Methods
public:
	Simulation() { }
	Simulation(SIMULATOR sim) : m_sim(sim) { }

	int Init()
	{
		if (InitInternal() != G_OK)
		{
			return G_ERR;
		}

		// Initialize
		m_renderer.Init(_window, m_sim.WindowSize());

		m_inputHandler = InputHandler::GetInstance();
		m_inputHandler->Init(_window);
		
		m_sim.Init();

		return G_OK;
	}

	void Run()
	{
		Start();
		// Loop until the user closes the window
		while (!glfwWindowShouldClose(_window))
		{
			m_timer.ReadTime();
			Update(m_timer.DT);
			Render();

			// Poll for and process events
			glfwPollEvents();
		}
		Stop();
	}

private:
	void Start() { }
	void Stop()
	{
		glfwTerminate();
	}

	void Update(float dt)	
	{
		m_inputHandler->Update(dt);
		m_sim.Update(dt); 
	}
	void Render()			
	{ 
		m_renderer.Render(m_sim); 
	}


	int InitInternal(){

		// Initialize the library
		if (!glfwInit())
			return G_ERR;

		// Window will not be resizeable
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		// Create a windowed mode window and its OpenGL context
		_window = glfwCreateWindow(m_sim.WindowSize().X, m_sim.WindowSize().Y, m_sim.WindowTitle().c_str(), NULL, NULL);
		if (!_window)
		{
			glfwTerminate();
			return G_ERR;
		}

		// Make the window's context current
		glfwMakeContextCurrent(_window);

		// Disable VSync
		//glfwSwapInterval(0);

		return G_OK;
	}
};

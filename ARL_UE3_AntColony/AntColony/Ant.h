#pragma once
#include "AntColony\HexMap\MapTileIndex.h"
#include <list>

//#define ANT_UPDATE_TIME 1.0f
//#define ANT_UPDATE_TIME 0.0001f
//#define ANT_UPDATE_TIME 0.0000001f
#define ANT_UPDATE_TIME 0.0f


typedef std::list<MapTileIndex> HexMapPath;


struct Ant
{
	HexMapPath m_path;
	bool m_searchingFood;
	float m_updateTimer;
	bool m_isAlive;


	float m_propPheromon;
	float m_propGeometry;

public:
	Ant() { }

	void Reset(const MapTileIndex& home)
	{
		m_path.clear();
		m_path.push_back(home);
		m_searchingFood = true;
		m_isAlive = true;
		ResetTimer();
	}
	void ResetTimer() 
	{
		m_updateTimer = ANT_UPDATE_TIME; 
	}


	MapTileIndex CurrentPosition() const 
	{ 
		return m_path.back();
	}
	MapTileIndex LastPosition() const 
	{
		if (m_path.size() == 1) { return CurrentPosition(); }
		auto it = m_path.end();
		--it;
		return *it;
	}

	void GoToMapTileIndex(const MapTileIndex idx)
	{
		m_path.push_back(idx);
	}

	size_t Steps() const 
	{
		return m_path.size();
	}
};


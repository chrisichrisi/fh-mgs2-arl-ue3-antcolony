#include "AntColonySim.h"
#include "..\RenderHelper.h"
#include "..\Input\InputHandler.h"
#include "..\MouseClickHelper.h"


AntColonySim::AntColonySim()
{
}


AntColonySim::~AntColonySim()
{
}









void AntColonySim::Init()
{
	m_colony.Init();
}
void AntColonySim::Update(float dt)
{
	ReadMouseInput();
	m_colony.Update(dt);
}
void AntColonySim::Render()
{
	Vector2f middle;
	int rows = m_colony.m_map.Rows();
	int cols = m_colony.m_map.Cols();

	float edgeWidth = 1.0f;

	Vector2f tileSize;
	tileSize.X = 2 * edgeWidth;
	tileSize.Y = sqrt(3.0f) * edgeWidth;

	Vector2f distances;
	distances.X = (3.0f / 4.0f) * tileSize.X;
	distances.Y = tileSize.Y / 2.0f;

	Vector2f mapSize;
	mapSize.X = cols * distances.X;
	mapSize.Y = rows * tileSize.Y;

	Vector2f startPoint;
	startPoint = Vector2f::SMinus(middle, Vector2f::SDivide(mapSize, 2.0f));

	

	RenderAnts(startPoint, distances, tileSize);
	RenderHexMap(startPoint, distances, tileSize);
}



void AntColonySim::RenderHexMap(const Vector2f& startPoint, const Vector2f& distances, const Vector2f& tileSize)
{
	int rows = m_colony.m_map.Rows();
	int cols = m_colony.m_map.Cols();

	Vector2f drawpos;
	MapTileIndex i = MapTileIndex(0, 0);

	for (; i.Row < rows; i.Row++)
	{
		i.Col = 0;
		for (; i.Col < cols; i.Col++)
		{
			drawpos.X = i.Col * distances.X;
			drawpos.Y = i.Row * tileSize.Y;

			drawpos = Vector2f::SPlus(drawpos, startPoint);
			if (i.Col % 2 != 0)
			{
				drawpos.Y = drawpos.Y - distances.Y;
			}
			RenderHelper::RenderHexMapTile(m_colony.m_map.GetMapTile(i), drawpos);
		}
	}
}

void AntColonySim::RenderAnts(const Vector2f& startPoint, const Vector2f& distances, const Vector2f& tileSize)
{
	Vector2f drawpos;

	for (UINT32 i = 0; i < m_colony.m_antCount; ++i)
	{
		MapTileIndex idx = m_colony.m_ants[i].CurrentPosition();
		drawpos.X = idx.Col * distances.X;
		drawpos.Y = idx.Row * tileSize.Y;

		drawpos = Vector2f::SPlus(drawpos, startPoint);
		if (idx.Col % 2 != 0)
		{
			drawpos.Y = drawpos.Y - distances.Y;
		}
		RenderHelper::RenderCircle(drawpos, Color::s_red);
	}
}

void AntColonySim::ReadMouseInput()
{
	KeyState ks = InputHandler::GetInstance()->GetKeyState();
	MouseState ms = InputHandler::GetInstance()->GetMouseState();


	// Check Mouse Input
	if (ms.IsPressed(GLFW_MOUSE_BUTTON_LEFT) || ms.IsPressed(GLFW_MOUSE_BUTTON_RIGHT))
	{
		MapTile::Enum tile = MapTile::Grass;
		if (ks.IsPressed(GLFW_KEY_LEFT_ALT) || ks.IsPressed(GLFW_KEY_RIGHT_ALT))
		{ // Change tile
			tile = MapTile::Food;
		}
		if (ks.IsPressed(GLFW_KEY_LEFT_CONTROL) || ks.IsPressed(GLFW_KEY_RIGHT_CONTROL))
		{ // Change tile
			tile = MapTile::Unpassable;
		}


		ChangeTile(ms.GetWorldPosition(), tile);
	}
}

void AntColonySim::ChangeTile(const Vector2f& worldPos, MapTile::Enum tile)
{
	MapTileIndex i = MouseClickHelper::GetClickedHexagon(m_colony.m_map, worldPos, Vector2f::s_zero);
	//MapTileIndex i;
	if (m_colony.m_map.InsideBoundaries(i))
	{
		//_ai->Map().Set(i, tile);

		m_colony.m_map.SetMapTile(i, tile);
		//_ai->MapChanged();
	}
}


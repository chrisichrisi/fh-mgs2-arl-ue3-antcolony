#pragma once

//--------------------------------------------------------------------------------------
// Includes
//--------------------------------------------------------------------------------------
#include "MapTile.h"
#include <vector>
#include "MapTileIndex.h"
#include <map>
#include <list>

//--------------------------------------------------------------------------------------
// Typedefs
//--------------------------------------------------------------------------------------
//typedef std::vector<MapTileIndex> HexMapIndices;
typedef std::list<MapTileIndex> HexMapIndices;



struct PheromonEdge
{
	MapTileIndex Index;
	float Value;
};

struct PheromonEdges
{
	std::vector<PheromonEdge> m_edges;

	void AddNeighbour(const MapTileIndex& idx)
	{
		for (auto it = m_edges.begin(); it != m_edges.end(); ++it)
		{
			if (it->Index == idx) { break; }
		}

		PheromonEdge e;
		e.Index = idx;
		e.Value = 0.0f;
		m_edges.push_back(e);
	}

	float GetValue(const MapTileIndex& idx)
	{
		for (auto it = m_edges.begin(); it != m_edges.end(); ++it)
		{
			if (it->Index == idx) { return it->Value; }
		}
		return 0.0f;
	}


	void SetValue(const MapTileIndex& idx, float value)
	{
		for (auto it = m_edges.begin(); it != m_edges.end(); ++it)
		{
			if (it->Index == idx) 
			{ 
				it->Value = value; 
				return; 
			}
		}
	}


	void AddValue(const MapTileIndex& idx, float value)
	{
		for (auto it = m_edges.begin(); it != m_edges.end(); ++it)
		{
			if (it->Index == idx)
			{
				it->Value += value;
				return;
			}
		}
	}
};


template <UINT32 ROW_COUNT, UINT32 COL_COUNT>
class HexMap
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
private:
	MapTile::Enum m_map[ROW_COUNT * COL_COUNT];
	PheromonEdges m_pheromonMap[ROW_COUNT * COL_COUNT];


	int m_rows = ROW_COUNT;
	int m_cols = COL_COUNT;
	UINT32 m_size = ROW_COUNT * COL_COUNT;

public:
	HexMap()
	{
		UINT32 i = 0;
		for (int r = 0; r < m_rows; ++r)
		{
			for (int c = 0; c < m_cols; ++c)
			{
				m_map[i] = MapTile::Grass;
				auto ns = GetNeighbourIndices(r, c);

				for (auto it = ns.begin(); it != ns.end(); ++it)
				{
					m_pheromonMap[i].AddNeighbour(*it);
				}
				++i;
			}
		}
	}
	

	// Get row count
	inline int Rows() const { return ROW_COUNT; }
	// Get col count
	inline int Cols() const { return COL_COUNT; }
	// Get element count
	inline int Size() const { return m_size; }

	// Get the tile at a specific index
	inline MapTile::Enum GetMapTile(const MapTileIndex& ind) const	{ return m_map[GetIndex(ind)]; }
	inline MapTile::Enum GetMapTile(int row, int col) const			{ return m_map[GetIndex(row, col)]; }

	inline PheromonEdges GetPheromons(const MapTileIndex& ind) const	{ return m_pheromonMap[GetIndex(ind)]; }
	inline PheromonEdges GetPheromons(int row, int col) const			{ return m_pheromonMap[GetIndex(row, col)]; }
	inline PheromonEdges GetPheromons(int index) const					{ return m_pheromonMap[index]; }

	// Set the tile at a specific index
	inline void SetMapTile(const MapTileIndex& ind, MapTile::Enum t) { m_map[GetIndex(ind)] = t; }
	inline void SetMapTile(int row, int col, MapTile::Enum t)		{ m_map[GetIndex(row, col)] = t; }

	inline void SetPheromon(const MapTileIndex& from, const MapTileIndex& to, float value) { m_pheromonMap[GetIndex(from)].SetValue(to, value); }
	inline void SetPheromon(int row, int col, const MapTileIndex& to, float value)		{ m_pheromonMap[GetIndex(row, col)].SetValue(to, value); }
	inline void SetPheromon(int index, const MapTileIndex& to, float value) { m_pheromonMap[index].SetValue(to, value); }

	inline void AddPheromon(const MapTileIndex& from, const MapTileIndex& to, float value) { m_pheromonMap[GetIndex(from)].AddValue(to, value); }
	// Check if index is inside boundaries
	inline bool InsideBoundaries(const MapTileIndex& ind) const { return ind.Row >= 0 && ind.Row < ROW_COUNT && ind.Col >= 0 && ind.Col < COL_COUNT; }
	inline bool InsideBoundaries(int row, int col) const		{ return row >= 0 && row < ROW_COUNT && col >= 0 && col < COL_COUNT; }


	// Translate 2D-Index to 1D-Index
	inline int GetIndex(const MapTileIndex& ind) const	{ return GetIndex(ind.Row, ind.Col); }
	inline int GetIndex(int row, int col) const			{ return (row * COL_COUNT) + col; }



public:

	HexMapIndices GetNeighbourIndices(const MapTileIndex& index) const { return GetNeighbourIndices(index.Row, index.Col); }
	// Get indices of neighbour tiles
	HexMapIndices GetNeighbourIndices(int row, int col) const
	{
		HexMapIndices neighbours;

		if (row + 1 < ROW_COUNT) { neighbours.push_back(MapTileIndex(row + 1, col)); }
		if (row - 1 >= 0) { neighbours.push_back(MapTileIndex(row - 1, col)); }
		if (col + 1 < COL_COUNT) { neighbours.push_back(MapTileIndex(row, col + 1)); }
		if (col - 1 >= 0) { neighbours.push_back(MapTileIndex(row, col - 1)); }

		if (col % 2 == 0)
		{
			if (row + 1 < ROW_COUNT)
			{
				if (col + 1 < COL_COUNT) { neighbours.push_back(MapTileIndex(row + 1, col + 1)); }
				if (col - 1 >= 0) { neighbours.push_back(MapTileIndex(row + 1, col - 1)); }
			}
		}
		else
		{
			if (row - 1 >= 0)
			{
				if (col + 1 < COL_COUNT) { neighbours.push_back(MapTileIndex(row - 1, col + 1)); }
				if (col - 1 >= 0) { neighbours.push_back(MapTileIndex(row - 1, col - 1)); }
			}
		}
		return neighbours;
	}

};



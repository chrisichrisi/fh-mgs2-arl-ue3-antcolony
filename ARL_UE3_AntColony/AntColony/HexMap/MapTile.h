#pragma once
#include "..\..\Core\typedefs.h"

struct MapTile
{
	enum Enum : UINT8
	{
		Unpassable,
		Grass,
		Nest,
		Food
	};
};

#pragma once

class MapTileIndex
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
public:
	int Row;
	int Col;

public:
	MapTileIndex() { }
	MapTileIndex(int row, int col) : Row(row), Col(col) { }


	static int GetDistance(const MapTileIndex& a, const MapTileIndex& b)
	{
		int colsDelta = a.Col - b.Col;
		int rowsDelta = a.Row - b.Row;
		colsDelta = colsDelta < 0 ? colsDelta * -1 : colsDelta;
		rowsDelta = rowsDelta < 0 ? rowsDelta * -1 : rowsDelta;
		return colsDelta + rowsDelta;
	}


	bool MapTileIndex::operator==(const MapTileIndex &v) const
	{
		if (&v == this) { return true; }
		if (Row != v.Row) { return false; }
		if (Col != v.Col) { return false; }
		return true;
	}
	bool MapTileIndex::operator!=(const MapTileIndex &v) const { return !(*this == v); }


	// Operators for comparison (because of use as key in map)
	bool operator<(MapTileIndex const& rhs) const	{ return (Row * 200 + Col) < (rhs.Row * 200 + rhs.Col); }
	bool operator<=(MapTileIndex const& rhs) const	{ return (*this == rhs || *this < rhs); }
	bool operator>(MapTileIndex const& rhs) const	{ return !(*this < rhs); }
	bool operator>=(MapTileIndex const& rhs) const	{ return (*this == rhs || *this > rhs); }
};


#include <vector>
typedef std::vector<MapTileIndex> MapPath;

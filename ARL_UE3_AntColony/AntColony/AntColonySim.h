#pragma once

#include <string>
#include "../Core/Math/Vector2.h"
#include "AntColony.h"

#define WIN_TITLE "Ant Colony"
#define WIN_WIDTH 1400
#define WIN_HEIGHT 700



class AntColonySim
{
private:
	Vector2i m_windowSize = Vector2i(WIN_WIDTH, WIN_HEIGHT);
	const std::string m_windowTitle = WIN_TITLE;


	AntColony m_colony;


public:
	AntColonySim();
	~AntColonySim();




	void Init();
	void Update(float dt);
	void Render();



private:
	void RenderHexMap(const Vector2f& startPoint, const Vector2f& distances, const Vector2f& tileSize);
	void RenderAnts(const Vector2f& startPoint, const Vector2f& distances, const Vector2f& tileSize);


	void ChangeTile(const Vector2f& worldPos, MapTile::Enum tile);
	void ReadMouseInput();

public:

	const Vector2i WindowSize() const { return m_windowSize; }
	std::string WindowTitle() const { return m_windowTitle; }
};


#pragma once

#include "include.h"
#include "HexMap\MapTileIndex.h"
#include "HexMap\HexMap.h"
#include "Ant.h"


#define MAP_ROWS 15
#define MAP_COLS 40


typedef HexMap<MAP_ROWS, MAP_COLS> AntHexMap;

//#define ANT_COUNT 10
#define ANT_COUNT 5


// 0 <= EVAPORATION_COEFFICIENT < 1
#define EVAPORATION_COEFFICIENT 0.3f

#define MIN_PHEROMON_PROBABILITY 0.0f
#define MAX_PHEROMON_PROBABILITY 1.0f

#define MIN_GEOMETRY_PROBABILITY 0.0f
#define MAX_GEOMETRY_PROBABILITY 1.0f


class AntColony
{
private:
	float m_propEvaporation = EVAPORATION_COEFFICIENT;

	MapTileIndex m_home;

public:
	AntHexMap m_map;

public:
	UINT32 m_antCount = ANT_COUNT;
	Ant m_ants[ANT_COUNT];

public:
	AntColony();
	~AntColony();

	void Init();
	void Update(float dt);


	bool Traverse(float dt);
	void UpdatePheromons();


	bool UpdateAnt(float dt, Ant& a, int i);
	void MoveAnt(Ant& a);

	MapTileIndex ChooseEdge(Ant& a, const HexMapIndices& ns);
	void RemoveEdges(HexMapIndices& neighbors, const Ant& a) const;

	
	void SetHome(const MapTileIndex& home)
	{
		m_map.SetMapTile(m_home, MapTile::Grass);
		m_map.SetMapTile(home, MapTile::Nest);
		m_home = home;
	}
};

#include "AntColony.h"
#include "..\Core\Random.h"


AntColony::AntColony()
{
	m_home = MapTileIndex(7, 5);

	SetHome(m_home);
	MapTileIndex foodIndex(7, 13);
	m_map.SetMapTile(foodIndex, MapTile::Food);


	for (UINT32 i = 0; i < m_antCount; ++i)
	{
		m_ants[i].m_propGeometry = Randomizer.GetFloat(MIN_GEOMETRY_PROBABILITY, MAX_GEOMETRY_PROBABILITY);
		m_ants[i].m_propPheromon = Randomizer.GetFloat(MIN_PHEROMON_PROBABILITY, MAX_PHEROMON_PROBABILITY);
	}

	GED_LOG(LogVerbosity::Info, 1, "All ants initialized");
}


AntColony::~AntColony()
{
}

void AntColony::Init()
{
	for (UINT32 i = 0; i < m_antCount; ++i)
	{
		m_ants[i].Reset(m_home);
	}
}


void AntColony::Update(float dt)
{
	bool allSolutionsFound;
	allSolutionsFound = Traverse(dt);

	if (allSolutionsFound)
	{
		UpdatePheromons();
		Init();
	}
}

bool AntColony::Traverse(float dt)
{
	bool allSolutionsFound = true;
	for (UINT32 i = 0; i < m_antCount; ++i)
	{
		Ant& a = m_ants[i];
		bool r = UpdateAnt(dt, a, i);
		if (!r) { allSolutionsFound = false; }
	}

	return allSolutionsFound;
}
void AntColony::UpdatePheromons()
{
	GED_LOG(LogVerbosity::Info, 1, "Ant-SIM: Marking and evaporating paths...");
	
	// Calculate the length of an optimal path

	auto mapSize = m_map.Size();
	std::vector<PheromonEdges> edges;
	edges.resize(mapSize);

	for (UINT32 i = 0; i < m_antCount; ++i)
	{
		// This ant found no food
		if (!m_ants[i].m_isAlive) { continue; }

		// Get optimal length between food and home
		int lengthOfOptimalPath = MapTileIndex::GetDistance(m_ants[i].CurrentPosition(), m_home);
		// Length of the path found by ant
		int lengthOfPath = m_ants[i].Steps();
		// Value to increment pheromon for this ant
		float pheromonInc = (float)lengthOfOptimalPath / (float)lengthOfPath;


		for (auto it = m_ants[i].m_path.begin(); it != m_ants[i].m_path.end(); ++it)
		{
			auto nit = std::next(it, 1);
			if (nit == m_ants[i].m_path.end()) { break; } // No more edges

			int index = m_map.GetIndex(*it);
			MapTileIndex to = *nit;
			edges[index].AddNeighbour(to);
			edges[index].AddValue(to, pheromonInc);
		}
	}


	// Now we gonna evaporate all pheromons
	for (int i = 0; i < mapSize; ++i)
	{
		// Loop throu the map tiles and get current
		auto currentPheromonTile = m_map.GetPheromons(i);
		auto pathPheromons = edges[i];

		for (auto nit = currentPheromonTile.m_edges.begin(); nit != currentPheromonTile.m_edges.end(); ++nit)
		{
			// Loop throu the edges for this edge
			auto edge = *nit;
			float pathValue = pathPheromons.GetValue(edge.Index);
			float currentValue = currentPheromonTile.GetValue(edge.Index);
			float newValue = (1 - m_propEvaporation) * currentValue + pathValue;
			m_map.SetPheromon(i, edge.Index, newValue);
		}
	}

	GED_LOG(LogVerbosity::Info, 1, "Ant-SIM: Paths marked and evaporated.");
}


bool AntColony::UpdateAnt(float dt, Ant& a, int i)
{
	a.m_updateTimer -= dt;
	if (a.m_updateTimer > 0) { return false; }
	if (!a.m_searchingFood)  { return true; }

	a.ResetTimer();
	//GED_LOG(LogVerbosity::Info, 1, "Ant_%i: Updating", i);

	MoveAnt(a);
	//GED_LOG(LogVerbosity::Info, 1, "Ant_%i: New position is (Row: %i, Col: %i)", i, m_ants[i].CurrentPosition().Row, m_ants[i].CurrentPosition().Col);


	auto type = m_map.GetMapTile(a.CurrentPosition());
	if (type == MapTile::Food)
	{
		GED_LOG(LogVerbosity::Info, 1, "Ant_%i: Found food within %i steps.", i, a.Steps());
		a.m_searchingFood = false;
		return true;
	}
	return false;
}

void AntColony::MoveAnt(Ant& a)
{
	auto curPos = a.CurrentPosition();
	HexMapIndices ns = m_map.GetNeighbourIndices(curPos);
	RemoveEdges(ns, a);
	if (ns.size() == 0)
	{
		a.m_isAlive = false;
		a.m_searchingFood = false;
		return;
	}

	auto nextTile = ChooseEdge(a, ns);
	a.GoToMapTileIndex(nextTile);
}


MapTileIndex AntColony::ChooseEdge(Ant& a, const HexMapIndices& ns)
{
	auto curPos = a.CurrentPosition();
	auto pheromons = m_map.GetPheromons(curPos);

	float divisor = 0.0f;

	// tau = pheromon
	// eta = geometry

	std::vector<float> probabilities;
	std::vector<float> values;
	probabilities.resize(ns.size());
	values.resize(ns.size());
	

	int i = 0;
	for (auto it = ns.begin(); it != ns.end(); ++it)
	{
		float tau, eta;
		auto idx = *it;

		// Calculate tau
		auto pheromonValue = m_map.GetPheromons(curPos).GetValue(idx);
		tau = pow(pheromonValue, a.m_propPheromon);

		// Calculate eta
		// Take the distance to the next tile...in hexagon this is always 1
		eta = pow(1.0f, a.m_propGeometry);

		
		values[i] = tau * eta;
		divisor += values[i];

		++i;
	}


	// Choose a random edge depending on the probability
	if (divisor != 0.0f)
	{
		i = 0;
		float val = 0.0f;
		float randomVal = Randomizer.GetFloat();
		for (auto it = ns.begin(); it != ns.end(); ++it)
		{
			values[i] = values[i] / divisor;

			if (randomVal <= values[i])
			{
				return *it;
			}

			++i;
		}
	}
	

	// Choose randomly, because all edges have the same probability
	int ri = Randomizer.GetInt(0, ns.size() - 1);
	MapTileIndex randomIdx = *(std::next(ns.begin(), ri));
	return randomIdx;
}



void AntColony::RemoveEdges(HexMapIndices& neighbors, const Ant& a) const
{
	neighbors.remove(a.LastPosition());

	for (auto it = neighbors.begin(); it != neighbors.end();)
	{
		if (m_map.GetMapTile(*it) == MapTile::Unpassable)
		{
			it = neighbors.erase(it);
		}
		else
		{
			++it;
		}
	}

	for (auto it = a.m_path.begin(); it != a.m_path.end(); ++it)
	{
		neighbors.remove(*it);
	}
}





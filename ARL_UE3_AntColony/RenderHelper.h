#pragma once


#include "Core\typedefs.h"
#include "Rendering\Color.h"
#include "AntColony\HexMap\MapTileIndex.h"
#include "AntColony\HexMap\MapTile.h"
#include "Core\Math\Vector2.h"

enum RenderShape
{
	RS_POINT,
	RS_TRIANGLE,
	RS_QUAD,
	RS_HEXAGON,
	RS_CIRCLE,
};

class RenderHelper
{
public:
	static void RenderRS(RenderShape rs, const Vector2f& pos = Vector2f::s_zero, const Color& c = Color::s_white, const Vector2f& scale = Vector2f::s_one);


	static void RenderHexagon(const Vector2f& pos, const Color& c);
	static void RenderTriangle(const Vector2f& pos, const Color& c);
	static void RenderQuad(const Vector2f& pos, const Color& c);
	static void RenderHexMapTile(const MapTile::Enum& tile, const Vector2f& pos);

	static void RenderPoint(const Vector2f& pos, const Color& c);
	static void RenderCircle(const Vector2f& pos, const Color& c);

	static void RenderPointByVertex();
	static void RenderTriangleByVertices();
	static void RenderQuadByVertices();
	static void RenderHexagonByVertices(const Color& c);
	static void RenderCircleByVertices(int points);
	static void RenderCircleLineByVertices(int points);
};


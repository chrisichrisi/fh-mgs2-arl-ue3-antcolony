#pragma once

#include "Core/defines.h"
#include "Core/typedefs.h"



#ifdef IDE_VISUAL_STUDIO
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#include <windows.h> // for OutputDebugStringA
#endif


// Library Includes
// Include GLEW and GLFW
#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>


// Includes from the Project
#include "Core/Logging/Logging.h"
#include "Core/Math/Vector2.h"

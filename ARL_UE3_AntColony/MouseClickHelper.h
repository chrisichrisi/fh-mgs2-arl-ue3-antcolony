

#include "include.h"


struct MouseClickHelper
{
	template <class HEXMAP>
	static MapTileIndex GetClickedHexagon(const HEXMAP& map, const Vector2f& mouseClick, const Vector2f& middle)
	{
		MapTileIndex i = MapTileIndex(-1, -1);
		//float edgeWidth = 1.0f;
		float edgeWidth = 1.0f;


		Vector2f tileSize;
		tileSize.X = 2 * edgeWidth;
		tileSize.Y = (sqrt(3.0f) * edgeWidth);

		Vector2f distances;
		distances.X = ((3.0f / 4.0f) * tileSize.X);
		distances.Y = (tileSize.Y / 2.0f);

		Vector2f mapSize;
		mapSize.X = (map.Cols() * distances.X);
		mapSize.Y = (map.Rows() * tileSize.Y);

		Vector2f startPoint;
		//startPoint = middle - (mapSize / 2.0f);
		startPoint = Vector2f::SMinus(middle, Vector2f::SDivide(mapSize, 2.0f));


		Vector2f mouseCorrection = Vector2f(edgeWidth / 2.0f, tileSize.Y / 2.0f);
		Vector2f mc = Vector2f::SMinus(mouseClick, Vector2f::SPlus(startPoint, mouseCorrection));
		//auto mc = mouseClick - startPoint + mouseCorrection;

		
		i.Col = (int)(mc.X / distances.X);
		if (i.Col % 2 != 0)
		{
			mc.Y = (mc.Y + distances.Y);
		}
		i.Row = (int)(mc.Y / tileSize.Y);
		return i;
	}
};
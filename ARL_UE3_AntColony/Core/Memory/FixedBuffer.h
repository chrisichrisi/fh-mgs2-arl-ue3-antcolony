#pragma once


//************************************
// Class FixedBuffer
//************************************
template <class T, int size>
struct FixedBuffer
{
	T m_buffer[size];
	int m_size = size;
};

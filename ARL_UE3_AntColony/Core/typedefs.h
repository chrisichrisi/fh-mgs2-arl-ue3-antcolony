#pragma once

//************************************
// Includes
//************************************
#include <stdint.h>

//************************************
// Typedefs
//************************************
// Signed integer types
typedef int64_t INT64;
typedef int32_t INT32;
typedef int16_t INT16;
typedef int8_t  INT8;


// Unsigned integer types
typedef uint64_t UINT64;
typedef uint32_t UINT32;
typedef uint16_t UINT16;
typedef uint8_t  UINT8;

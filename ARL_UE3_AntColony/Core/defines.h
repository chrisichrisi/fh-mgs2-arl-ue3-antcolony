#pragma once

//************************************
// Defines
//************************************
#ifdef _DEBUG
#define DEBUG_MODE
#else
#define RELEASE_MODE
#endif



#ifdef _MSC_VER
#define IDE_VISUAL_STUDIO
#endif

#ifdef IDE_VISUAL_STUDIO
// Prevents Visual studio from throwing errors because of the non safe versions of methods (like vsnprintf/vsnprintf_s)
#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif
#endif



#define G_OK 0
#define G_ERR -1


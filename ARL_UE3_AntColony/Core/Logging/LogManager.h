#pragma once


//************************************
// Includes
//************************************
#include <list>
#include "LogVerbosity.h"
#include "CallInfo.h"
#include "Log.h"


//************************************
// Forward declaration BaseLogger
//************************************
class BaseLogger;


//************************************
// Class LogManager
//************************************
class LogManager
{
	std::list<BaseLogger*> m_loggers;

public:
	void Register(BaseLogger* logger);
	void Unregister(BaseLogger* logger);
	void Log(const LogVerbosity::Enum verbosity, const CallInfo callInfo, LogChannel channel, const char* format, ...);
};


//************************************
// Extern Globals
//************************************
extern LogManager gLogManager;


//************************************
// Class BaseLogger
//************************************
class BaseLogger
{
public:
	BaseLogger()
	{
		gLogManager.Register(this);
	}

	virtual ~BaseLogger()
	{
		gLogManager.Unregister(this);
	}

	virtual void Log(const LogVerbosity::Enum verbosity, const CallInfo& callInfo, LogChannel channel, const char* format, va_list args) = 0;
};

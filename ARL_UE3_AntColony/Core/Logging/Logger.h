/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:05
filename: 	Core\Logging\Logger.h
file path:	Core\Logging
file base:	Logger
file ext:	h
author:		Christoph Kappler

purpose:	Provides a simple, extendable Logging-Interface
*********************************************************************/
#pragma once

//************************************
// Includes
//************************************
#include <stdio.h>
#include <stdarg.h>
#include "Log.h"
#include "LogManager.h"


extern LogManager gLogManager;
//************************************
// Macros
//************************************
#ifdef LOGGING_ENABLED
	#define GED_NEW_LOGGER(name, TYPE, ...) TYPE name = TYPE(__VA_ARGS__);
	//#define GED_LOG(logger, verbosity, channel, format, ...) logger.Log(verbosity, CallInfo(__FILE__, __FUNCTION__, __LINE__), channel, format, __VA_ARGS__)
	#define GED_LOG(verbosity, channel, format, ...) gLogManager.Log(verbosity, CallInfo(__FILE__, __FUNCTION__, __LINE__), channel, format, __VA_ARGS__)
#else
	#define GED_NEW_LOGGER(name, TYPE, ...)
	//#define GED_LOG(logger, verbosity, channel, format, ...) 
	#define GED_LOG(verbosity, channel, format, ...) 
#endif





//************************************
// Class Logger
//************************************
template <class OutputterClass, class FormatterClass, class FilterClass>
class Logger : BaseLogger
{
private:
	FilterClass m_filter;
	FormatterClass m_formatter;
	OutputterClass m_outputter;

public:
	Logger() { }
	Logger(OutputterClass& outputter) :
		m_outputter(outputter), m_formatter(FormatterClass()), m_filter(FilterClass())
	{	}
	Logger(FormatterClass& formatter) :
		m_outputter(OutputterClass()), m_formatter(formatter), m_filter(FilterClass())
	{	}
	Logger(FilterClass& filter) :
		m_outputter(OutputterClass()), m_formatter(FormatterClass()), m_filter(filter)
	{	}
	Logger(OutputterClass& outputter, FormatterClass& formatter, FilterClass& filter) :
		m_outputter(outputter), m_formatter(formatter), m_filter(filter)
	{	}


	//************************************
	// Method:    Log
	// FullName:  Logger<OutputterClass, FormatterClass, FilterClass>::Log
	// Access:    public 
	// Returns:   void
	// Qualifier:
	// Parameter: LogVerbosity verbosity Level of verbosity for the message
	// Parameter: LogCallInfo callInfo Additional information from the caller
	// Parameter: LogChannel channel Channel the message belongs to
	// Parameter: const char * format Format of the message
	// Parameter: ...
	//************************************
	virtual void Log(const LogVerbosity::Enum verbosity, const CallInfo& callInfo, LogChannel channel, const char* format, va_list args)
	{
		if (m_filter.PassedFilter(verbosity, channel))
		{
			FixedLogCharBuffer buffer;
			m_formatter.Format(buffer, verbosity, callInfo, channel, format, args);
			m_outputter.Output(buffer);
		}
	}
};


/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:00
filename: 	Core\Logging\LogVerbosity.h
file path:	Core\Logging
file base:	LogVerbosity
file ext:	h
author:		Christoph Kappler

purpose:	Specifies the levels of verbosity
*********************************************************************/
#pragma once

//************************************
// Includes
//************************************
#include "..\typedefs.h"

//************************************
// Macros
//************************************


//************************************
// Enum LogVerbosity
//************************************
struct LogVerbosity
{
	enum Enum : UINT8
	{
		Info,
		Warning,
		Error,
		Fatal
	};

	//************************************
	// Method:    verbosityToString
	// FullName:  verbosityToString
	// Access:    public 
	// Returns:   const char*
	// Qualifier:
	// Parameter: const LogVerbosity & verbosity
	// Purpose:
	//	Converts a verbosity level to a string
	//************************************
	static const char* VerbosityToString(const LogVerbosity::Enum& verbosity)
	{
		if (verbosity == Info)			{ return "Info"; }
		else if (verbosity == Warning) { return "Warning"; }
		else if (verbosity == Error)	{ return "Error"; }
		else if (verbosity == Fatal)	{ return "Fatal"; }
		else return "";
	}
};

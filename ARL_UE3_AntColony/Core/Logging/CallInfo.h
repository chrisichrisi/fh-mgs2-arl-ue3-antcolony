/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:07
filename: 	Core\Logging\CallInfo.h
file path:	Core\Logging
file base:	LogCallInfo
file ext:	h
author:		Christoph Kappler

purpose:	Stores information about where the log call came from
*********************************************************************/
#pragma once

//************************************
// Struct CallInfo
//************************************
struct CallInfo
{
	const char* m_file;
	const char* m_func;
	int m_lineNumber;


	//************************************
	// Method:    CallInfo
	// FullName:  CallInfo::CallInfo
	// Access:    public 
	// Qualifier: : m_file(file), m_func(func), m_lineNumber(lineNumber)
	// Parameter: const char * file
	// Parameter: const char * func
	// Parameter: int lineNumber
	//************************************
	CallInfo(const char* file, const char* func, int lineNumber) :
		m_file(file),
		m_func(func),
		m_lineNumber(lineNumber)
	{
	}
};
